FROM ubuntu:bionic

ENV TEXMFHOME=/root/texmf
COPY texmf /root/texmf

RUN ln -snf /usr/share/zoneinfo/Australia/Adelaide /etc/localtime \
    && echo "Australia/Adelaide" > /etc/timezone \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install make wget python texlive-latex-base texlive-latex-extra texlive-bibtex-extra biber texlive-fonts-recommended xzdec -y \
    && rm -rf /var/lib/apt/lists/* \
    && wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py && rm get-pip.py \
    && pip install pygments \
    && texhash /root/texmf

WORKDIR /opt/run
